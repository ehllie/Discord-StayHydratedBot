import datetime


class UserData:
    """A class used for managing timers and preferences of users"""
    def __init__(self, channel, user_id):
        self.__id = user_id
        self.__response_ch = channel
        self.__water_drunk = 0
        self.__reminded_times = 0
        self.__interval = datetime.timedelta(seconds=3600)
        self.__muted = False
        self.__next_sip = datetime.datetime.now()
        self.__reminder_id = 0

    def __eq__(self, other):
        if isinstance(other, UserData):
            return other.__id == self.__id
        elif isinstance(other, int):
            return other == self.__id
        else:
            return False

    def __hash__(self):
        return int(self.__id)

    def get_id(self):
        return self.__id

    def update(self):
        self.__reminder_id = self.__reminder_id + 1
        self.__next_sip = datetime.datetime.now() + self.__interval

    def get_reminder_id(self):
        return self.__reminder_id

    def change_interval(self, new_interval):
        self.__interval = new_interval

    def reminded(self):
        self.__reminded_times = self.__reminded_times + 1

    def sipped(self):
        self.__water_drunk = self.__water_drunk + 1

    def times_reminded(self):
        return self.__reminded_times

    def times_sipped(self):
        return self.__water_drunk

    def toggle_mute(self):
        self.__muted = not self.__muted

    def set_channel(self, new_channel):
        self.__response_ch = new_channel

    def get_channel(self):
        return self.__response_ch

    def is_muted(self):
        return self.__muted

    def next_sip(self):
        return self.__next_sip
