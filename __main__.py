import discord
from discord.ext.commands import Bot
import sys
from discord.ext import commands
import configparser
import UserData
from collections import namedtuple
import asyncio
import datetime
import pickle
import random
import datetime_formatting

VERSION = "0.2"

# Reads the bot.ini file
parser = configparser.ConfigParser()
try:
    parser.read("bot.ini")
    prefix = parser.get("defaults", "prefix")
    token = parser.get("defaults", "token")
    owner_id = int(parser.get("defaults", "owner_id"))
except configparser.NoSectionError:
    parser["defaults"] = {"prefix": "h!", "token": "<your token goes here>", "owner_id": "<your id goes here>"}
    with open("bot.ini", "w") as configfile:
        parser.write(configfile)
    sys.exit("Please fill out the bot.ini file")

# Reads the data.pickle file
try:
    with open("data.pickle", "rb") as datafile:
        users, allowed_channels = pickle.load(datafile)
except (FileNotFoundError, EOFError) as e:
    with open("data.pickle", "wb") as datafile:
        users = dict()
        allowed_channels = dict()
        pickle.dump([users, allowed_channels], datafile)


def save():
    with open("data.pickle", "wb") as file:
        pickle.dump([users, allowed_channels], file)


# Initiates the bot
Client = discord.Client()
bot = commands.Bot(command_prefix=prefix)


def is_dm(channel: discord.TextChannel):
    return isinstance(channel, discord.DMChannel)


async def auto_save():  # Background auto_save
    while not bot.is_closed():
        save()
        await asyncio.sleep(60)


async def remind(user: UserData.UserData, reminder_id):
    if user.next_sip() > datetime.datetime.now():
        sleep_time = user.next_sip() - datetime.datetime.now()
        sleep_time = sleep_time.seconds + sleep_time.days * 3600
        await asyncio.sleep(sleep_time)
        if user.get_reminder_id() == reminder_id and not user.is_muted():
            channel: discord.TextChannel = bot.get_channel(user.get_channel())
            message = "Remember to stay hydrated <@{}>!".format(user.get_id())
            await channel.send(message)
            user.reminded()


@bot.event
async def on_ready():
    bot.loop.create_task(auto_save())
    for user_id, user in users.items():
        await remind(user, user.get_reminder_id())
    print("Bot is ready!")


# Commands available to everyone
@bot.command(name="sip", pass_context=True, brief="Tells the bot you've hydrated yourself",
             description=("Can be followed by how much time you want between each time you want the bot to "
                          + "remind you to drink next, for example {}sip 2h".format(prefix)))
# Used by users to indicate they've drank and when to be reminded
async def sip(ctx: commands.Context, *time):
    channel: discord.TextChannel = ctx.channel
    if not is_dm(channel):
        if channel.id not in allowed_channels[ctx.guild.id]:
            return

    if ctx.author.id in users.keys():
        user = users[ctx.author.id]
    else:
        user = UserData.UserData(channel.id, ctx.author.id)
        users[ctx.author.id] = user

    if len(time) != 0:
        time = datetime_formatting.read_timedelta(list(time))
        user.change_interval(time)

    user.set_channel(ctx.channel.id)
    user.sipped()
    user.update()
    time = user.next_sip() - datetime.datetime.now()
    message = ("You will be reminded to stay hydrated in {} {}".format(
               datetime_formatting.neat_timedelta(time), ctx.author.name))
    await ctx.send(message)
    await remind(user, user.get_reminder_id())


@bot.command(name="total", pass_context=True, brief="Displays your tallies",
             description=("Shows you how many times you've ran drank water and how many times the bot "
                          + "reminded you to do so"))
# Used by users to check how many times they've drank
async def total(ctx: commands.Context):
    channel: discord.TextChannel = ctx.channel
    if not is_dm(channel):
        if channel.id not in allowed_channels[channel.guild.id]:
            return

    if ctx.author.id in users.keys():
        user = users[ctx.author.id]
    else:
        user = UserData.UserData(channel.id, ctx.author.id)
        users[ctx.author.id] = user
    sips = user.times_sipped()
    reminds = user.times_reminded()
    message = "You have used the {}sip command {} times\nThe bot has reminded you to stay hydrated {} times".format(
        prefix, sips, reminds)
    await ctx.send(message)


@bot.command(name="stop", pass_context=True, brief="Stops the bot from messaging you",
             description="Use this command if you don't want the bot to be reminding you to stay hydrated")
# Used by users to prevent the bot from sending them messages
async def stop(ctx: commands.Context):
    channel: discord.TextChannel = ctx.channel
    if not is_dm(channel):
        if channel.id not in allowed_channels[channel.guild.id]:
            return

    if ctx.author.id in users.keys():
        user = users[ctx.author.id]
    else:
        user = UserData.UserData(channel.id, ctx.author.id)
        users[ctx.author.id] = user
    user.toggle_mute()
    if user.is_muted():
        insert = "now "
    else:
        insert = "no longer "
    message = "Bot messages to you are {}muted".format(insert)
    await ctx.send(message)


# Commands available only to moderators
@bot.command(name="allow_c", pass_context=True, brief="Toggle bot responses in a channel",
             description="Bot will only respond to commands in designated channels, use this command in a channel"
                         + " you want the bot to be responding in to toggle it")
# Used by moderators to toggle this channel
@commands.has_permissions(manage_channels=True)
async def allow_c(ctx: commands.Context):
    guild = ctx.guild.id
    channel = ctx.channel.id
    if guild in allowed_channels.keys():
        if channel in allowed_channels[guild]:
            allowed_channels.get(guild).remove(channel)
            await ctx.send("Removed <#{}> from the list of allowed channels".format(channel))
        else:
            allowed_channels.get(guild).add(channel)
            await ctx.send("Added <#{}> to the list of allowed channels".format(channel))
    else:
        allowed_channels[guild] = {channel}
        await ctx.send("Added <#{}> to the list of allowed channels".format(channel))


# Commands available only to the bot owner
@bot.command(name="shutdown", pass_context=True)  # Used by the bot owner to safely shut down the bot
async def shutdown(ctx: commands.Context):
    if ctx.message.author.id == owner_id:
        save()
        await ctx.send("Saving data and shutting down...")
        await bot.logout()
    else:
        await ctx.send("This command can only be executed by the bot owner")
        return


bot.run(token)
